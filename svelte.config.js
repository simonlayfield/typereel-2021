import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-vercel';

const config = {
	kit: {
		adapter: adapter({
			external: ['@napi-rs/*']
		})
	},

	preprocess: [
		preprocess({
			postcss: true
		})
	]
};

export default config;
