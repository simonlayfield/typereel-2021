import { v4 as uuidv4 } from 'uuid';
import { connectToDatabase } from '$lib/db';
import { ObjectId } from 'mongodb';
import User from '../routes/user/_user_model';
import Specimen from '../routes/specimen/_specimen_model';
import Session from '../routes/user/_session_model';
import bcrypt from 'bcryptjs';

const { VITE_MJ_APIKEY_PUBLIC, VITE_MJ_APIKEY_PRIVATE, VITE_DEVELOPMENT_MODE, VITE_MAILERSEND_TOKEN } = import.meta.env;
const SPECIMENS_PER_PAGE = import.meta.env.VITE_SPECIMENS_PER_PAGE;

export const getUserByEmail = async (email) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const user_collection = db.collection('users');
    console.log('kjskjskjskjskjskjs');
    console.log(email);
	return await user_collection.findOne({ email: email });
};

export const getUserById = async (id) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const user_collection = db.collection('users');
	return await user_collection.findOne({ _id: ObjectId(id) });
};

export const sendUserNotificationEmails = async (users, comment, commenter, post) => {
	const developmentMode = VITE_DEVELOPMENT_MODE == 'development';

    let messages = [];

	for (var user of users) {
		messages.push({
            from: {
                email: 'simon@typereel.io',
                name: 'Typereel'
            },
            to: [
                {
                    email: user.email
                }
            ],
            subject: 'A user has responded on Typereel',
            text: "<p>Hey {$receiver}, A comment has been made on a Typereel post to which you're subscribed: https://typereel.io{$post}. If you'd like to stop receiving these emails, simply visit the specimen page and uncheck 'Notify me of comments on this specimen'. Keep on reelin', Typereel",
            html: "<p>Hey {$receiver},</p><p>A comment has been made on a <a href='https://typereel.io{$post}'>Typereel post</a> to which you're subscribed.</p><p>{$sender} says:</p><p>{$comment}</p><p>If you'd like to stop receiving these emails, simply visit the specimen page and uncheck 'Notify me of comments on this specimen'.</p><p>Keep on reelin',</p><p>Typereel</p>",
            variables: [
                {
                    email: user.email,
                    substitutions: [
                        {
                            var: 'sender',
                            value: commenter.username
                        },
                        {
                            var: 'receiver',
                            value: user.username
                        },
                        {
                            var: 'comment',
                            value: comment
                        },
                        {
                            var: 'post',
                            value: post
                        }
                    ]
                }
            ]
        });
	}

    try {
		const emailRequest = await fetch('https://api.mailersend.com/v1/bulk-email', {
			method: 'POST',
			headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  VITE_MAILERSEND_TOKEN
            },
			body: JSON.stringify(messages)
		});

        return emailRequest;

	} catch (err) {
		console.log(err);
	}

};

export const sendUserMentionEmails = async (users, comment, commenter, post) => {
	const developmentMode = VITE_DEVELOPMENT_MODE == 'development';

    let messages = [];

	for (var user of users) {
		messages.push({
            from: {
                email: 'simon@typereel.io',
                name: 'Typereel'
            },
            to: [
                {
                    email: user.email
                }
            ],
            subject: 'A user has mentioned you on Typereel',
            text: "Hey {$receiver}, A user has mentioned you in a comment they made on a Typereel post: https://typereel.io{$post}.",
            html: "<p>Hey {$receiver},</p><p>A user has mentioned you in a comment they made on a <a href='https://typereel.io{$post}'>Typereel post</a>.</p><p>{$sender} says:</p><p>{$comment}</p>",
            variables: [
                {
                    email: user.email,
                    substitutions: [
                        {
                            var: 'sender',
                            value: commenter.username
                        },
                        {
                            var: 'receiver',
                            value: user.username
                        },
                        {
                            var: 'comment',
                            value: comment
                        },
                        {
                            var: 'post',
                            value: post
                        }
                    ]
                }
            ]
        });
	}

    try {
		const emailRequest = await fetch('https://api.mailersend.com/v1/bulk-email', {
			method: 'POST',
			headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  VITE_MAILERSEND_TOKEN
            },
			body: JSON.stringify(messages)
		});

        return emailRequest;

	} catch (err) {
		console.log(err);
	}

};

export const sendUserResetEmail = async (email, username, hash) => {
	const developmentMode = VITE_DEVELOPMENT_MODE == 'development';

    try {
		const emailRequest = await fetch('https://api.mailersend.com/v1/email', {
			method: 'POST',
			headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  VITE_MAILERSEND_TOKEN
            },
			body: JSON.stringify({
				from: {
					email: 'simon@typereel.io',
					name: 'Typereel'
				},
				to: [
					{
						email: email
					}
				],
				subject: 'Password reset request',
                template_id: 'zr6ke4n19j9gon12',
				variables: [
					{
                        email: email,
						substitutions: [
							{
								var: 'resetPasswordUsername',
								value: username
							},
							{
								var: 'resetPasswordHash',
								value: hash
							}
						]
					}
				]
			})
		});

        return emailRequest;

	} catch (err) {
		console.log(err);
	}

};

export const getSpecimenSubscribers = async (type, id) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const specimen_collection = db.collection('specimens');
	const feature_collection = db.collection('inspiration');
	const collection = type == 'feature' ? feature_collection : specimen_collection;
	const specimen = await collection.findOne({ _id: ObjectId(id) });
	return specimen.subscribers;
};

export const getUsersByEmail = async (emails = []) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const user_collection = db.collection('users');
	return await user_collection.find({ email: { $in: emails } }).toArray();
};

export const getUsersByUsername = async (usernames = []) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const user_collection = db.collection('users');
	return await user_collection.find({ username: { $in: usernames } }).toArray();
};

export const getUserByUsername = async (username = '') => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const user_collection = db.collection('users');
	return await user_collection.findOne({ username: username });
};

export const registerUser = async (user) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const user_collection = db.collection('users');

	const existingUser = await user_collection.findOne({ email: user.email });
	if (existingUser) return Promise.reject(new Error('User already exists'));

	var newUser = new User(
		{
			_id: new ObjectId(),
			username: user.username,
			password: bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null),
			email: user.email,
			role: 'user',
			verified: false
		},
		{
			collection: 'users'
		}
	);

	return await newUser.save();
};

export const uploadSpecimen = async (data) => {
    var newSpecimen = new Specimen(
		data
	);

	return await newSpecimen.save();

};

export const createSession = async (user) => {
	var session = new Session(
		{
			_id: new ObjectId(),
			session_id: uuidv4(),
			email: user.email,
			username: user.username,
            verified: user.verified,
            role: user.role
		},
		{
			collection: 'sessions'
		}
	);

	return await session.save();
};

export const getSessionByEmail = async (email) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const session_collection = db.collection('sessions');
	return await session_collection?.findOne({ email: email });
};

export const getSession = async (id) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const session_collection = db.collection('sessions');
	return await session_collection?.findOne({ session_id: id });
};

export const removeSession = async (id) => {
	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const session_collection = db.collection('sessions');
	return await session_collection?.deleteOne({ session_id: id });
};

export const addUserResetHash = async (user) => {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const user_collection = db.collection('users');
		const resetPasswordHash = uuidv4();
		const updatedUser = await user_collection
			.findOneAndUpdate(
				{ email: user.email },
				{ $set: { reset_password_hash: resetPasswordHash } },
				{ upsert: true, returnDocument: 'after' }
			)
			.then((user) => {
				return user;
			});

		return await sendUserResetEmail(
			updatedUser.value.email,
			updatedUser.value.username,
			updatedUser.value.reset_password_hash
		);
	} catch (err) {
		console.log(err);
	}
};

export const resetUserPassword = async (username, password, hash) => {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const user_collection = db.collection('users');
		const updatedUser = await user_collection.findOneAndUpdate(
			{ username: username, reset_password_hash: hash },
			{
				$set: {
					password: bcrypt.hashSync(password, bcrypt.genSaltSync(10), null)
				},
				$unset: {
					reset_password_hash: ''
				}
			},
			{
				upsert: true,
				returnDocument: 'after'
			}
		);

		return updatedUser;
	} catch (err) {
		console.log(err);
	}
};

export const verifyUser = async (id) => {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const user_collection = db.collection('users');
		const sessions_collection = db.collection('sessions');
        let user_email = null;
		await user_collection
			.findOneAndUpdate(
				{ _id: ObjectId(id) },
				{ $set: { verified: true } },
				{ upsert: true, new: true }
			)
			.then((user) => {
				user_email = user.value.email
			});

		await sessions_collection
			.findOneAndUpdate(
				{ email: user_email },
				{ $set: { verified: true } },
				{ upsert: true, new: true }
			)
			.then((session) => {
				return session;
			});

	} catch (err) {
		console.log(err);
	}
};

export const sendUserVerifyEmail = async (user) => {
    
	try {
		const emailRequest = await fetch('https://api.mailersend.com/v1/email', {
			method: 'POST',
			headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  VITE_MAILERSEND_TOKEN
            },
			body: JSON.stringify({
				from: {
					'email': 'simon@typereel.io',
					'name': 'Typereel'
				},
				to: [
					{
						email: `${user.email}`
					}
				],
				subject: 'Verify your email address',
                template_id: 'v69oxl5e8o2l785k',
				variables: [
					{
                        email: `${user.email}`,
						substitutions: [
							{
								var: 'user_id',
								value: `${user.id}`
							}
						]
					}
				]
			})
		});

        return emailRequest;

	} catch (err) {
		console.log(err);
	}

};

export const sendUserRegistrationEmail = async (email) => {
	const developmentMode = VITE_DEVELOPMENT_MODE == 'development';
    
    try {

        const emailRequest = await fetch('https://api.mailersend.com/v1/email', {
			method: 'POST',
			headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +  VITE_MAILERSEND_TOKEN
            },
			body: JSON.stringify({
				from: {
					email: 'simon@typereel.io',
					name: 'Typereel'
				},
				to: [
					{
						email: email
					}
				],
				subject: 'Thanks for registering',
                template_id: 'z86org8e9oegew13'
			})
		});

        return emailRequest;

    } catch (err) {
        console.log(err);
    }

};

export const getInspirationByTag = async function(tags, pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const inspiration_collection = db.collection('inspiration');

    try {
        const specimens = await inspiration_collection.find({ tags: { $in: tags } })
        .sort({ $natural: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();

        return specimens;

    } catch (error) {
        console.log(error)
    }
}

export const getInspiration = async function(pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const inspiration_collection = db.collection('inspiration');
    
    try {
        const specimens = await inspiration_collection
        .find()
        .sort({ $natural: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();
        
        return specimens;
        
    } catch (error) {
        console.log(error)
    }
}

export const getInspirationByTagCount = async function(tags) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const inspiration_collection = db.collection('inspiration');

    try {
        const count = await inspiration_collection.countDocuments({ tags: { $in: tags } });

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getInspirationCount = async function() {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const inspiration_collection = db.collection('inspiration');

    try {
        const count = await inspiration_collection.countDocuments();

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getSpecimens = async function(pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');
    
    try {
        const specimens = await collection
        .find()
        .sort({ created_at: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();
        
        return specimens;
        
    } catch (error) {
        console.log(error)
    }
}

export const getIdentifiedSpecimens = async function(pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');
    
    try {
        const specimens = await collection
        .find({name: { $ne: null }})
        .sort({ created_at: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();
        
        return specimens;
        
    } catch (error) {
        console.log(error)
    }
}

export const getUnidentifiedSpecimens = async function(pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');
    
    try {
        const specimens = await collection
        .find({name: null})
        .sort({ created_at: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();
        
        return specimens;
        
    } catch (error) {
        console.log(error)
    }
}

export const getSpecimensByTag = async function(tags, pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const specimens = await collection.find({ tags: { $in: tags } })
        .sort({ created_at: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();

        return specimens;

    } catch (error) {
        console.log(error)
    }
}

export const getSpecimensByTagCount = async function(tags) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const count = await collection.countDocuments({ tags: { $in: tags } });

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getIdentifiedSpecimensByTagCount = async function(tags) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const count = await collection.countDocuments({ name: { $ne: null }, tags: { $in: tags } });

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getSpecimensByFont = async function(fonts, pageNumber = 1) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const specimens = await collection.find({ "identified.name": { $in: fonts } })
        .sort({ created_at: -1 })
        .limit(parseInt(SPECIMENS_PER_PAGE))
        .skip(parseInt(SPECIMENS_PER_PAGE) * (parseInt(pageNumber) - 1))
        .toArray();

        return specimens;

    } catch (error) {
        console.log(error)
    }
}

export const getSpecimensByFontCount = async function(fonts) {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const count = await collection.countDocuments({ identified: { $in: fonts } });

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getSpecimensCount = async function() {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const count = await collection.countDocuments();

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getIdentifiedSpecimensCount = async function() {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const count = await collection.count({ name: { $ne: null } });

        return count;

    } catch (error) {
        console.log(error)
    }
}

export const getUnidentifiedSpecimensCount = async function() {
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const collection = db.collection('specimens');

    try {
        const count = await collection.count({ name: null });

        return count;

    } catch (error) {
        console.log(error)
    }
}