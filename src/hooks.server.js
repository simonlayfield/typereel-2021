import cookie from 'cookie';
import { getSession } from './lib/_db';

export const handle = async ({ event, resolve }) => {

	const cookies = cookie.parse(event.request.headers.get('cookie') || '');

	// TODO https://github.com/sveltejs/kit/issues/1046
	if (event.url.searchParams.has('_method')) {
		event.request.method = event.url.searchParams.get('_method').toUpperCase();
	}

	if (cookies.session_id) {
        const session = await getSession(cookies.session_id);
		if (session) {
            event.locals.user = { 
                email: session.email,
                username: session.username,
                verified: session.verified,
                role: session.role,
            };
		}
	} else {
        event.locals.user = null;
    }
    
    const response = await resolve(event);
    return response;
};