import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';
import { ObjectId } from 'mongodb';

export async function GET(req) {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('inspiration');
		const specimen = await collection.findOne({_id: ObjectId(req.params.id)});
        
        // If we have identified fonts in our db
        if (specimen.identified) {
            // Create new array of font names to fetch from db
            // Must be sorted alphabetically to match returned db data
            let fontNames = specimen.identified
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((font) => {
                return font.name;
            });
            
            const font_collection = db.collection('fonts');
          const fonts = await font_collection
          .find({ name: { $in: fontNames } })
          .sort({ name: 1 })
          .toArray();

          specimen.fonts = fonts;

        }

		return json({
			data: specimen
		});
	} catch (err) {
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}

export async function POST(req) {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('inspiration');
		const specimens = await collection.find().toArray();

		return json({
			data: specimens
		});
	} catch (err) {
		console.log(err);
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
