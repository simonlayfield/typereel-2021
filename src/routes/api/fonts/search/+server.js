import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

export async function GET(event) {

    // If font family names are specified in the request then
    // we can return only those, otherwise return all
    const searchText = event.url.searchParams.get('text');
    const regexText = new RegExp(`^${searchText}`);

	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('fonts');

        let fonts;

        fonts = await collection.find({ "name": { $regex: regexText, $options: 'i' }}).limit(10).toArray();

		return json({
			data: fonts
		});
	} catch (err) {
		return json({
			error: err.message
		}, {
			status: 500
		});
	}
}