import { json as json$1 } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

export async function GET(event) {

    // If font family names are specified in the request then
    // we can return only those, otherwise return all
    const fontNameArray = event.url.searchParams.get('families')?.split(',');

	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('fonts');

        let fonts;

        if (fontNameArray) {
            fonts = await collection.find({ name: { $in: fontNameArray } }).sort({name: 1}).toArray();
        } else {
            fonts = await collection.find().sort({name: 1}).toArray();
        }

		return json$1({
			data: fonts
		});
	} catch (err) {
		return json$1({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}

export async function POST({ request }) {
	const data = await request.json();

	let query;

	// We have to rebuild the query structure for MongodDB
	if (data.length) {
		const queryItems = [];
		// For each object passed from the app
		data.forEach((character) => {
			queryItems.push({
				'attributes.characters': {
					$elemMatch: { name: character['character'], attributes: character['selected_attribute'] }
				}
			});
		});
		query = {
			$and: queryItems
		};
	} else {
		query = {};
	}

	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('fonts');

		const fonts = await collection.find(query).sort({name: 1}).toArray();

		return json$1({
			data: fonts
		});
	} catch (err) {
		return json$1({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
