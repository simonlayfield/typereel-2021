import { json } from '@sveltejs/kit';
import {
    getSpecimensByTag,
    getSpecimensByFont,
    getSpecimens,
    getSpecimensCount,
    getSpecimensByTagCount,
    getIdentifiedSpecimensByTagCount,
    getSpecimensByFontCount,
    getIdentifiedSpecimensCount,
    getIdentifiedSpecimens,
    getUnidentifiedSpecimens,
    getUnidentifiedSpecimensCount,
} from '$lib/_db';

const SPECIMENS_PER_PAGE = import.meta.env.VITE_SPECIMENS_PER_PAGE;

export async function GET(request) {

    const uri = new URLSearchParams(request.url.search)
    const pageNumber = uri.get('page') || 1;
    const view = uri.get('view') || 1;
    const tags = uri.get('tag');
    const fonts = uri.get('fonts');
    const tagsArray = tags?.split(',');
    const fontsArray = fonts?.split(',');

    try {

        let specimens;
        let collectionCount;
        let identifiedCount;

        if (view && view === 'identified') {
            specimens = await getIdentifiedSpecimens(pageNumber);
            collectionCount = await getIdentifiedSpecimensCount();
        } else if (view && view === 'unidentified') {
            specimens = await getUnidentifiedSpecimens(pageNumber);
            collectionCount = await getUnidentifiedSpecimensCount();
        } else if (tagsArray?.length) {
            specimens = await getSpecimensByTag(tagsArray, pageNumber);
            collectionCount = await getSpecimensByTagCount(tagsArray);
            identifiedCount = await getIdentifiedSpecimensByTagCount(tagsArray);
        } else if (fontsArray?.length) {
            specimens = await getSpecimensByFont(fontsArray, pageNumber);
            collectionCount = await getSpecimensByFontCount(fontsArray);
            identifiedCount = null;
        } else {
            specimens = await getSpecimens(pageNumber);
            collectionCount = await getSpecimensCount();
            identifiedCount = await getIdentifiedSpecimensCount(specimens);
        }

        
        const pageTotal = Math.ceil( collectionCount / parseInt(SPECIMENS_PER_PAGE));

    return json({
  data: specimens,
  pageTotal: pageTotal,
  specimensPerPage: SPECIMENS_PER_PAGE,
  currentPageNumber: pageNumber,
  collectionCount: collectionCount,
  identifiedCount: identifiedCount,
})

  } catch (err) {
    return json({
  error: 'A server error occurred'
}, {
      status: 500
    })
  }

};

export async function POST({ request }) {

  try {

    const specimens = await inspiration_collection.find().toArray();
  
    return json({
  data: specimens
})

  } catch (err) {
    console.log(err);
    return json({
  error: 'A server error occurred'
}, {
      status: 500
    })
  }

};