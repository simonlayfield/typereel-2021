import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';
import { ObjectId } from 'mongodb';

export async function GET(request) {

	try {

        const dbConnection = await connectToDatabase();
        const db = dbConnection.db;
        const specimens_collection = db.collection('specimens');

		const specimens = await specimens_collection
			.find({})
			.sort({ _id: -1 })
            .toArray();

        const modifiedSpecimens = specimens.map((specimen) => {

            const timestamp = new ObjectId(specimen._id).getTimestamp();

            specimen.created_at = {
                "$date": timestamp
            };
            
            specimen.modified_at = {
                "$date": timestamp
            };
            
            specimen._id = {
                $oid: specimen._id
            };

            return specimen;

        });

		return json({
			...modifiedSpecimens
		});
	} catch (err) {
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
