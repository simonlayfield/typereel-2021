import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

const SPECIMENS_PER_PAGE = import.meta.env.VITE_SPECIMENS_PER_PAGE;

export async function GET(request) {

    const pageNumber = request.params.number - 1;

	try {

        const dbConnection = await connectToDatabase();
        const db = dbConnection.db;
        const specimens_collection = db.collection('specimens');

        const collectionCount = await specimens_collection.countDocuments({});
        const pageTotal = Math.ceil( collectionCount / parseInt(SPECIMENS_PER_PAGE));
		const specimens = await specimens_collection
			.find({})
			.skip(parseInt(SPECIMENS_PER_PAGE) * parseInt(pageNumber))
			.limit(parseInt(SPECIMENS_PER_PAGE))
			.sort({ created_at: -1 })
            .toArray()
		return json({
			data: specimens,
            pageTotal: pageTotal,
            specimensPerPage: SPECIMENS_PER_PAGE
		});
	} catch (err) {
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
