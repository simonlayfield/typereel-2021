import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

export async function GET(event) {

	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const specimens_collection = db.collection('specimens');

		const identifiedCount = await specimens_collection.aggregate([
			{
				$match: {
                    $and: [{ 'comments.type': 'font', 'comments.user': event.params.username }]
				}
			},
			{
				$group: {
					_id: null,
					count: {
						$sum: {
							$size: {
								$filter: {
									input: '$comments',
									as: 'el',
									cond: {
										$eq: ['$$el.type', 'font']
									}
								}
							}
						}
					}
				}
			}
		]).toArray();

        
		const identifiedData = {
			count: identifiedCount[0]?.count || 0
		};

		return json({
			identifiedData
		});
        
	} catch (err) {
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
