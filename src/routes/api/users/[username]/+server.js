import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

export async function GET(event) {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const users_collection = db.collection('users');

		const user = await users_collection.findOne({ username: event.params.username });

		if (user === null) {
			return json(
				{
					error: 'No username was found with the username provided'
				},
				{
					status: 500
				}
			);
		}

		const userData = {
			username: user.username
		};

		return json({
			data: userData
		});
	} catch (err) {
		console.log(err.message);
		return json(
			{
				error: 'A server error occurred'
			},
			{
				status: 500
			}
		);
	}
}
