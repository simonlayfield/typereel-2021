import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

export async function GET(request) {
	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('users');
		const users = await collection.find().toArray();

		return json({
			data: users
		});
	} catch (err) {
		return json(
			{
				error: 'A server error occurred'
			},
			{
				status: 500
			}
		);
	}
}
