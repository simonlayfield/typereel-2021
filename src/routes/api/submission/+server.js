import { json as json$1 } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';
import { uploadSpecimen } from '$lib/_db';
import { FormData } from 'formdata-node';
import { ObjectId } from 'mongodb';
import { getSession } from '$lib/_db';
import cookie from 'cookie';
import sha1 from 'sha1';

const { VITE_CLOUDINARY_URL, VITE_CLOUDINARY_KEY, VITE_CLOUDINARY_SECRET } = import.meta.env;


/** @type {import("@sveltejs/kit").RequestHandler} */
export async function POST({ request }) {
    
	const formData = await request.formData();
	const file = formData.get('specimenFile');
	const uploadedFile = await uploadFile(file, 1);
    const cookies = cookie.parse(request.headers.get('cookie') || '');
    const session = await getSession(cookies.session_id);

    if (!cookies.session_id) {
        // TODO There's no user
        // Redirect to login...
    }

	try {

		const newSpecimenObj = {
            _id: new ObjectId(),
			name: null,
			image: {
				src: `${uploadedFile.public_id}.${uploadedFile.format}`,
				width: uploadedFile.width,
				height: uploadedFile.height,
				average_colour: formData.get('averageColor'),
			},
			user: session.username,
			credit: [
				{
					name: formData.get('creditName') || null,
					url: formData.get('creditUrl') || null,
					type: formData.get('creditType') || null,
				}
			],
			fonts: [],
			subscribers: [session.username],
			comments: [],
            tags: formData.get('tags') ? formData.get('tags').split(', ') : []
		};

		if (formData.get('reference')) {
			const comment = {
				body: formData.get('name'),
				url: formData.get('reference'),
				user: session.username,
				type: 'match',
			};
			newSpecimenObj.comments.push(comment);
		}

        await uploadSpecimen(newSpecimenObj);

        return new Response(undefined, { status: 302, headers: { Location: '/thanks' } })
        
	} catch (err) {
		console.log(err);
		return json$1({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}

async function uploadFile(file) {

    const timestamp = Math.floor(new Date().getTime() / 1000);
    const signature = sha1(`folder=specimens&timestamp=${timestamp}${VITE_CLOUDINARY_SECRET}`); 

	let form = new FormData();
	form.append('file', file, file.name);
	form.append('folder', 'specimens');
	form.append('api_key', `${VITE_CLOUDINARY_KEY}`);
	form.append('timestamp', timestamp);
	form.append('signature', signature);

	var url = VITE_CLOUDINARY_URL;
	var response = await fetch(url, {
		method: 'POST',
		body: form
	});

	response = await response.json();
	return response;
}


    