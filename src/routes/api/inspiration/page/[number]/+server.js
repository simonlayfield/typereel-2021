import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';

const SPECIMENS_PER_PAGE = import.meta.env.VITE_SPECIMENS_PER_PAGE;

export async function GET(request) {    
    const pageNumber = request.params.number - 1;

	try {

		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('inspiration');
        const collectionCount = await collection.countDocuments({});
        const pageTotal = Math.ceil( collectionCount / parseInt(SPECIMENS_PER_PAGE));

		const specimens = await collection
			.find()
            .sort({$natural:-1})
			.limit(parseInt(SPECIMENS_PER_PAGE))
			.skip(parseInt(SPECIMENS_PER_PAGE) * parseInt(pageNumber))
			.toArray();

		return json({
			data: specimens.reverse(),
                pageTotal: pageTotal,
                specimensPerPage: SPECIMENS_PER_PAGE
		});
	} catch (err) {
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
