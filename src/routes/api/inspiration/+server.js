import { json } from '@sveltejs/kit';
import { getInspirationByTag, getInspiration, getInspirationCount, getInspirationByTagCount } from '$lib/_db';

const SPECIMENS_PER_PAGE = import.meta.env.VITE_SPECIMENS_PER_PAGE;

export async function GET(request) {

    const uri = new URLSearchParams(request.url.search)
    const pageNumber = uri.get('page') || 1;
    const tags = uri.get('tag');
    const tagsArray = tags?.split(',');

    try {

        let specimens;
        let collectionCount;

        if (tagsArray?.length) {
            specimens = await getInspirationByTag(tagsArray, pageNumber);
            collectionCount = await getInspirationByTagCount(tagsArray);
        } else {
            specimens = await getInspiration(pageNumber);
            collectionCount = await getInspirationCount();
        }
        
        const pageTotal = Math.ceil( collectionCount / parseInt(SPECIMENS_PER_PAGE));

    return json({
      data: specimens.reverse(),
      pageTotal: pageTotal,
      specimensPerPage: SPECIMENS_PER_PAGE,
      currentPageNumber: pageNumber,
      collectionCount: collectionCount,
    });

  } catch (err) {
    return json({
  error: 'A server error occurred'
}, {
      status: 500
    })
  }

};

export async function POST({ request }) {

  try {

    const specimens = await inspiration_collection.find().toArray();
  
    return json({
  data: specimens
})

  } catch (err) {
    console.log(err);
    return json({
  error: 'A server error occurred'
}, {
      status: 500
    })
  }

};