import { json } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';
import { ObjectId } from 'mongodb';

export async function GET(event) {

    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const daily_find_collection = db.collection('daily_find');
    const specimens_collection = db.collection('specimens');
    const font_collection = db.collection('fonts');

	try {
		const daily_find = await specimens_collection.findOne({}, {sort: { daily_find: -1 }});
		const specimen = await specimens_collection.findOne({_id: daily_find._id});

        // If we have identified fonts in our db
        if (specimen.identified) {

            // Create new array of font names to fetch from db
            // Must be sorted alphabetically to match returned db data
            let fontNames = specimen.identified
            .sort((a, b) => a.name.localeCompare(b.name))
            .map((font) => {
                return font.name;
            });
            
          const fonts = await font_collection
          .find({ name: { $in: fontNames } })
          .sort({ name: 1 })
          .toArray();

          specimen.fonts = fonts;

        }

		return json({
			data: specimen
		});
	} catch (err) {
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}

export async function POST(event) {
	try {
		const specimens = await specimens_collection.find().toArray();

		return json({
			data: specimens
		});
	} catch (err) {
		console.log(err);
		return json({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
