import { json as json$1 } from '@sveltejs/kit';
import { connectToDatabase } from '$lib/db';
import { ObjectId } from 'mongodb';

let db;
let fonts_collection;

async function initConnection() {}

export async function GET(request) {

	const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const collection = db.collection('fonts');

	try {
		const font = await collection.findOne({ _id: ObjectId(request.params.id) });

		return json$1({
			data: font
		});
	} catch (err) {
		return json$1({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}

export async function POST({ request }) {

    const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const collection = db.collection('fonts');

	const data = await request.json();

	let query;

	// We have to rebuild the query structure for MongodDB
	if (data.length) {
		const queryItems = [];
		// For each object passed from the app
		data.forEach((character) => {
			queryItems.push({
				'attributes.characters': {
					$elemMatch: { name: character['character'], attributes: character['selected_attribute'] }
				}
			});
		});
		query = {
			$and: queryItems
		};
	} else {
		query = {};
	}

	try {
		const fonts = await collection.find(query).toArray();

		return json$1({
			data: fonts
		});
	} catch (err) {
		return json$1({
			error: 'A server error occurred'
		}, {
			status: 500
		});
	}
}
