import { connectToDatabase } from '$lib/db';
import { getUsersByUsername, getSpecimenSubscribers, sendUserNotificationEmails, sendUserMentionEmails } from '$lib/_db.js';
import { ObjectId } from 'mongodb';

function linkify(text) {
	var exp = /(\b(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
}


export async function POST(event) {
    
    const collection_name = event.params.type == 'inspiration' ? 'inspiration' : 'specimens';
    const specimen_type = event.params.type == 'inspiration' ? 'feature' : 'specimen';
        
    const dbConnection = await connectToDatabase();
    const db = dbConnection.db;
    const specimen_collection = db.collection(collection_name);
    
    const request = event.request;
    const body = await request.formData();
	const specimenUrl = `/${specimen_type}/${event.params.id}`;

    
	let comment = body.get('comment'),
    context = specimen_type,
    commentType = body.get('type'),
    commentUrl = body.get('url');
    
	comment = comment.replace(/\n/g, '<br>\n');
	comment = linkify(comment);

	await specimen_collection.findOneAndUpdate(
		{ _id: ObjectId(event.params.id) },
		{
			$push: {
				comments: {
					body: comment,
					user: event.locals.user.username,
					url: commentUrl || null,
					type: commentType || 'text'
				}
			}
		},
		(err, result) => {
			console.log(err)
		}
	);


    // If a user was mentioned in the comment, send them an email
    const mentionedUsernames = commentType = body.get('mentions') ? body.get('mentions').split(',') : null;

    if (mentionedUsernames) {
        const mentionedUsers = await getUsersByUsername(mentionedUsernames);
        
        await sendUserMentionEmails(mentionedUsers, comment, event.locals.user, specimenUrl)
			.catch((err) => {
				console.log(err);
			});
    }

	const subscribers = await getSpecimenSubscribers(context, event.params.id);

    // Do not send an email to the person who is commenting
    const recipients = subscribers.filter(
		(subscribedUser) => subscribedUser !== event.locals.user.username
	);

	if (recipients && recipients.length) {
		const users = await getUsersByUsername(recipients);

		sendUserNotificationEmails(users, comment, event.locals.user, specimenUrl)
			.catch((err) => {
				console.log(err);
			});

            return new Response(undefined, { status: 302, headers: { Location: event.request.headers.get('referer') } })
	} else {
		// No subscribers
		return new Response(undefined, { status: 302, headers: { Location: event.request.headers.get('referer') } })
	}


}
