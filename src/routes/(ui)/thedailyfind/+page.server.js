import { error } from '@sveltejs/kit';

export async function load({ url, params, fetch, locals }) {

	const pageUrl = `/api/dailyfind`;
	const res = await fetch(pageUrl);
	const json = await res.json();
	const { data } = json;

	if (res.ok) {
		return {
			specimen: data,
			user: locals.user,
            returnToUrl: url.pathname,
		};
	}

	throw error(500, `Could not load ${pageUrl}`);
}
