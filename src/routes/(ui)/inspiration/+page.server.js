import { error } from '@sveltejs/kit';

export async function load({ params, url, fetch, locals, parent }) {

    await parent();
	const page_url = `/api/inspiration${url.search}`;
	const res = await fetch(page_url);
    const { data, pageTotal, specimensPerPage, currentPageNumber, collectionCount, identifiedCount } = await res.json();

	if (res.ok) {
		return {
			specimens: data.reverse(),
            user: locals.user || null,
            returnToUrl: url.pathname,
            pageTotal: pageTotal,
			paginationPageTotal: specimensPerPage,
            currentPageNumber: currentPageNumber,
            params: params,
            queryString: url.search,
            collectionCount: collectionCount,
            identifiedCount: identifiedCount,
		};
	}

	throw error(500, `Could not load ${url}`);
}
