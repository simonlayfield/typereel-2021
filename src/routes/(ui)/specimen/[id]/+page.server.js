import { error } from '@sveltejs/kit';

export async function load({ url, params, fetch, locals }) {

	const pageUrl = `/api/specimens/${params.id}`;
	const res = await fetch(pageUrl);
	const json = await res.json();
	const { data } = json;

	// let fonts = null;

    // if (data.identified) {
    //     // Also fetch relevant fonts
    //     const fontUrl = `/api/fonts?families=${encodeURIComponent(data.identified.join(','))}`;
    //     const fontRes = await fetch(fontUrl);
    //     const fontJson = await fontRes.json();
    //     fonts = fontJson.data;
    // }

    // data.fonts = fonts;

	if (res.ok) {
		return {
			specimen: data,
			user: locals.user,
            returnToUrl: url.pathname,
		};
	}

	throw error(500, `Could not load ${pageUrl}`);
}
