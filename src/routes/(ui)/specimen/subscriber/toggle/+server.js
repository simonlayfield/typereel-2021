import { connectToDatabase } from '$lib/db';
import { ObjectId } from 'mongodb';
import { getSession } from '$lib/_db';
import cookie from 'cookie';

export async function POST({ request }) {
        
    const cookies = cookie.parse(request.headers.get('cookie') || '');
    const session = await getSession(cookies.session_id);

	const body = await request.json();
	const username = session.username;
	const specimenId = body.specimen;

    if (body.subscribed) {
		removeSubscriber(username, specimenId)
	} else {
		addSubscriber(username, specimenId)
	}

    return new Response({"subscribed": true});
}

async function removeSubscriber(username, specimenId) {

    const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const specimens_collection = db.collection('specimens');

    try {
        const response =  await specimens_collection
        .findOneAndUpdate(
            { _id: ObjectId(specimenId) },
            {
                $pull: {
                    subscribers: username
                }
            }
        );
        return response;
    } catch (err) {
        console.log(err);
    }
}

async function addSubscriber(username, specimenId) {

    const dbConnection = await connectToDatabase();
	const db = dbConnection.db;
	const specimens_collection = db.collection('specimens');

    try {
        const response = await specimens_collection
        .findOneAndUpdate(
            { _id: ObjectId(specimenId) },
            {
                $push: {
                    subscribers: username
                }
            }
        );
        return response;
    } catch (err) {
        console.log(err);
    }
}