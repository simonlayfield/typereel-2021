import { getAverageColor } from 'fast-average-color-node';

const averageColor = async function(buffer) {

    try {

        let calculatedColor = await getAverageColor(buffer, {
            defaultColor: [0, 0, 0, 0]
        });
        
        return calculatedColor;
    } catch (error) {
        console.log(error);
    }

}

export default averageColor;