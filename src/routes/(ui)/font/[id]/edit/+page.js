import { redirect } from '@sveltejs/kit';


export async function load({ url, params, fetch, parent }) {

        const { user } = await parent();

    if (!user || user.role != 'admin') {
		throw redirect(302, '/');
	}

	const res = await fetch(`/api/font/${params.id}`);
	const attributeData = await fetch('../../data/attributes.json');
	const attributeJson = await attributeData.json();

	if (res) {
		return {
			font: await res.json(),
            attributes: attributeJson
		};
	}
}

