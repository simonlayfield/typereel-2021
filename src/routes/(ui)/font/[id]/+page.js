import { error } from '@sveltejs/kit';

export async function load({ params, fetch }) {
	const res = await fetch(`/api/font/${params.id}`);

	if (res) {
		return {
			font: await res.json()
		};
	}

	throw error(500, `Could not load ${url}`);
}
