export async function load({ fetch }) {
	const res = await fetch('/api/fonts');
	const jsonRes = await res.json();

	const attributeData = await fetch('data/attributes.json');
	const attributeJson = await attributeData.json();

	return { fonts: jsonRes.data, letters: attributeJson };
}
