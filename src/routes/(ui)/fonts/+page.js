export async function load({ fetch }) {
	const data = await fetch('/api/fonts');
	const fonts = await data.json();
        
	return {
		fonts: fonts.data
	};
}
