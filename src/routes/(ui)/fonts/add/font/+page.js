export async function load({ fetch }) {
	const attributeData = await fetch('../../data/attributes.json');
	const attributeJson = await attributeData.json();
	return { attributes: attributeJson };
}
