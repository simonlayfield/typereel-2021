import { connectToDatabase } from '$lib/db';

export async function POST({ request }) {

	const formData = await request.formData();

	// Form fields
    const name = formData.get('name'),
          url = formData.get('url'),
          foundry = formData.get('foundry'),
          foundry_url = formData.get('foundry_url'),
          vendor_names = formData.getAll('vendor_names'),
          vendor_urls = formData.getAll('vendor_urls'),
          characters = formData.getAll('characters'),
          attributes = formData.getAll('attributes');

	// Resructure vendors for the db

	const vendors = Object.values(vendor_names).map((value, index) => {
		return {
			name: value,
			url: vendor_urls[index]
		};
	});

	// Restructure font character attributes for the db

	let character_attributes = [];

	if (characters.length) {
		const font_attributes = {};

		characters.forEach((character, index) => {
			if (font_attributes[character]) {
				font_attributes[character].push(attributes[index]);
			} else {
				font_attributes[character] = [attributes[index]];
			}
		});

		character_attributes = Object.entries(font_attributes).map((entry) => {
			return {
				name: entry[0],
				attributes: entry[1]
			};
		});
	}

	try {
		const dbConnection = await connectToDatabase();
		const db = dbConnection.db;
		const collection = db.collection('fonts');

		await collection.insertOne({
			name: name,
			url: url,
			foundry: foundry,
			foundry_url: foundry_url,
			vendors: vendors,
			attributes: {
				weights: [],
				styles: [],
				characters: character_attributes
			}
		});

        return new Response(undefined, { status: 302, headers: { Location: '/thanks' } })

	} catch (err) {
		console.log(err);
	}
}
