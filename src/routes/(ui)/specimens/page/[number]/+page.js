import { error } from '@sveltejs/kit';

export async function load({ params, url, fetch, session, stuff }) {
	const page_url = `/api/specimens/page/${params.number}`;
	const res = await fetch(page_url);
	const { data, pageTotal, specimensPerPage } = await res.json();

	if (res.ok) {
		return {
			specimens: data,
			currentPageNumber: params.number,
			pageTotal: pageTotal,
			paginationPageTotal: specimensPerPage,
            user: session.user || null,
            returnToUrl: url.pathname,
		};
	}

	throw error(500, `Could not load ${url}`);
}
