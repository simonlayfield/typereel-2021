import { error } from '@sveltejs/kit';

export async function load({ url, params, fetch, locals }) {
    
	const page_url = `/api/features/${params.id}`;
	const res = await fetch(page_url);
	const json = await res.json();

	const { data } = json;

	if (res.ok) {
		return {
			specimen: data,
			user: locals.user || null
		};
	}

	throw error(500, `Could not load ${page_url}`);
}
