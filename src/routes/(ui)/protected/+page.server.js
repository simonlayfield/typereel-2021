import { redirect } from '@sveltejs/kit';

export async function load({ params, url, fetch, locals, parent }) {

    await parent();
    
	if (!locals.user) {
		throw redirect(302, '/login');
	}
	return {
		user: locals.user
	};
}
