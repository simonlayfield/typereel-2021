export async function load({url, locals, parent}) {

	return {
		user: locals.user,
        returnToUrl: url.pathname,
        includeHeader: true
	};
}