
export async function load({url, locals, parent}) {

    await parent();

	return {
		user: locals.user,
        returnToUrl: url.pathname,
        includeHeader: true
	};
}
