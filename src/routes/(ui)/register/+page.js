/** @type {import('@sveltejs/kit').PageLoad} */

export async function load({ params, url, fetch, parent }) {
    return {
            returnToUrl: url.searchParams.get('referrer')
		}
}
