import { redirect } from '@sveltejs/kit';

export async function load({ params, url, fetch, locals }) {

	if (!locals.user) {
		throw redirect(302, '/login?referrer=/submission');
	}
	return {
		user: locals.user
	};
}
