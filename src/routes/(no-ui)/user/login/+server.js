import { json as json$1 } from '@sveltejs/kit';
import { createSession, getSessionByEmail, getUserByEmail } from '$lib/_db';
import { serialize } from 'cookie';
import bcrypt from 'bcryptjs';

export async function POST({ request }) {

    const body = await request.json();
    const email = body.email;
    const password = body.password;
	const user = await getUserByEmail(email);

    if (!user) {
        return json$1({
			message: 'Incorrect user or password'
		}, {
        	status: 401
        });
    }

    let passwordHash = user.password;
    let passwordIsValid = null;

    const result = await bcrypt.compare(password, passwordHash);
    passwordIsValid = Boolean(result);

	if (!user || !result) {
		return json$1({
			message: 'Incorrect user or password'
		}, {
			status: 401
		});
	}

	try {

		const session = await getSessionByEmail(user.email);
        let session_id = session?.session_id;

        if (!session_id) {
            const new_session = await createSession(user);
            session_id = new_session.session_id;
        }

		return json$1({
			message: 'Successfully signed in'
		}, {
			status: 201,
			headers: {
				'Set-Cookie': serialize('session_id', session_id, {
					path: '/',
					httpOnly: true,
					sameSite: 'none',
					secure: process.env.NODE_ENV === 'production',
					maxAge: 60 * 60 * 24 * 7 // one week
				})
			}
		});
	} catch (err) {
        console.log(err);
		return json$1({
			message: err._message
		}, {
			status: 500
		});
	}
}