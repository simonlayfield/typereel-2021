import { removeSession } from '$lib/_db';
import { parse, serialize } from 'cookie';

export async function GET({ request }) {

    const cookies = parse(request.headers.get('cookie') || '');

    if (cookies.session_id) {
     await removeSession(cookies.session_id);
    }

    return new Response(JSON.stringify({ status: 200 }), {
        headers: {
            'Set-Cookie': serialize('session_id', '', {
             path: '/',
             expires: new Date(0),
         }),
        }
    });
}