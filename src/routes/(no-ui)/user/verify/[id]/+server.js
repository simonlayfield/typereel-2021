import { json } from '@sveltejs/kit';
import { verifyUser } from '$lib/_db';

export async function GET(request) {

	try {
        const userId = request.params.id;
		await verifyUser(userId);
                
		return new Response(undefined, { status: 302, headers: { Location: '/verified' } })
	} catch (err) {
        
		return json({
			message: err._message
		}, {
			status: 500
		});
        
	}
}
