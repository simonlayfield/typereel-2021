import { error } from '@sveltejs/kit';

export async function load({ url, params, fetch, parent }) {

    const { user } = await parent();
	const page_url = `/api/users/${params.username}`;
	const res = await fetch(page_url);
	const { data } = await res.json();
    const identifiedCountUrl = `/api/users/${params.username}/identified`;
	const identifiedResponse = await fetch(identifiedCountUrl);
	const { identifiedData } = await identifiedResponse.json();

	if (res.ok) {
    	return {
			username: data.username,
            user: user || null,
            returnToUrl: url.pathname,
            identifiedCount: identifiedData.count
		};
	} else if (res.status == '404') {
            return {
			errorMessage: 'We don\'t seem to have a user with that username'
		};
    }

	throw error(500, `Could not load ${url}`);
}
