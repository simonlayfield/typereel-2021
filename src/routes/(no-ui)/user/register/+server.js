import { json as json$1 } from '@sveltejs/kit';
const { VITE_DEVELOPMENT_MODE } = import.meta.env;
import { createSession, getUserByEmail, getUserByUsername, registerUser, sendUserVerifyEmail } from '$lib/_db';
import { serialize } from 'cookie';
import { redirect } from '@sveltejs/kit';

export async function POST({ request, url }) {
    
    try {

        const requestOrigin = request.headers.get("origin");

        if (requestOrigin !== url.origin) {
            throw error(401, `Access denied`);
        }        

        const body = await request.json();
            const username = body.username,
            email = body.email,
            password = body.password,
            token = body.token;

        /*
            A hidden field with a timestamp should exist for
            users with javascript enabled. We compare the timestamp
            to the time of submission. If there is no field value
            or the submission process took less than 3 seconds we
            determine the user as a bot and direct straight
            to the thank you page.
        */
       
        if (token) {
            const tokenTime = Number(token);
            const timeNow = Date.now();
            const timeDifference = timeNow - tokenTime;
            console.log(timeDifference);
            if (timeDifference < 500) {
                throw redirect(302, '/thanks');
            }
        } else {
            throw redirect(302, '/thanks');
        }

        const userByEmail = await getUserByEmail(email);
        const userByUsername = await getUserByUsername(username);

    	if (userByUsername) {
    		return json$1({
    			message: 'This username is taken'
    		}, {
    			status: 409
    		});
    	}

    	if (userByEmail) {
    		return json$1({
    			message: 'This email address is already registered'
    		}, {
    			status: 409
    		});
    	}

		const newUser = await registerUser({
			username,
			email,
			password
		});
        
		const { session_id } = await createSession(newUser);

        await sendUserVerifyEmail(newUser);
        
		return json$1({
			message: 'Successfully signed up'
		}, {
			status: 201,
			headers: {
				'Set-Cookie': serialize('session_id', session_id, {
					path: '/',
					httpOnly: true,
                    sameSite: 'none',
					secure: process.env.NODE_ENV === 'production',
					maxAge: 60 * 60 * 24 * 7 // one week
				})
			}
		});
	} catch (err) {
        
		return json$1({
			message: err._message
		}, {
			status: 500
		});
        
	}
}
