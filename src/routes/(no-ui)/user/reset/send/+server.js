import { json as json$1 } from '@sveltejs/kit';
import { getUserByEmail, addUserResetHash } from '$lib/_db';

export async function POST({ request }) {

    const body = await request.json();
    const email = body.email;
    console.log(body);
    const user = await getUserByEmail(email);

	if (!user) {
		return json$1({
			message: 'No user exists with email address'
		}, {
			status: 409
		});
	}

	try {

		const res = await addUserResetHash({
			email
		});

		return json$1({
			message: 'Email sent'
		});
	} catch (err) {
        
		return json$1({
			message: err._message
		}, {
			status: 500
		});
        
	}
}
