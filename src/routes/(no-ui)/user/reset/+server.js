import { json as json$1 } from '@sveltejs/kit';
import { resetUserPassword, getUserByUsername } from '$lib/_db';

export async function POST({ request }) {

    const body = await request.json();
    const username = body.username;
    const password = body.password;
    const hash = body.hash;
	const user = await getUserByUsername(username);

    if (!user) {
        return json$1({
			message: 'Incorrect user'
		}, {
        	status: 401
        });
    }

    if (hash != user.reset_password_hash) {
        return json$1({
			message: 'Reset password link is invalid'
		}, {
        	status: 401
        });
    }

	try {

        await resetUserPassword(username, password, hash);

        return new Response(undefined, { status: 302, headers: { Location: '/reset-complete' } })

	} catch (err) {
        console.log(err);
		return json$1({
			message: err._message
		}, {
			status: 500
		});
	}
}