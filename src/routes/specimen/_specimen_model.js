import mongoose from 'mongoose';

const { Schema } = mongoose

const SpecimenSchema = new Schema(
	{
		name: {
			type: null
		},
		image: {
			type: Object
		},
		credit: {
			type: Array
		},
		name: {
			type: String
		},
		user: {
			type: String
		},
		comments: {
			type: Array
		},
		subscribers: {
			type: Array
		},
		tags: {
			type: Array
		}
	},
	{
		timestamps: {
			createdAt: 'created_at',
			updatedAt: 'updated_at'
		}
	}
);

export default mongoose.model('Specimen', SpecimenSchema, 'specimens');
