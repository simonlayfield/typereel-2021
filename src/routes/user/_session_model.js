import mongoose from 'mongoose';

const SessionSchema = new mongoose.Schema({
	session_id: {
		type: String
	},
	email: {
		type: String
	},
	username: {
		type: String
	},
    verified: {
        type: Boolean
    },
    role: {
        type: String
    }
});

export default mongoose.model('Session', SessionSchema);
