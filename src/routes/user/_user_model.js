import mongoose from 'mongoose';
import validate from 'mongoose-validator';

const usernameValidator = [
  validate({
    validator: 'isLength',
    arguments: [2, 15],
    message: 'Usernames must be between {ARGS[0]} and {ARGS[1]} characters',
  }),
  validate({
    validator: 'isAlphanumeric',
    message: 'Usernames cannot contain special characters',
  }),
];

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    validate: usernameValidator,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user',
    required: true,
  },
  verified: {
    type: Boolean,
    default: false,
    required: true,
  }
});

export default mongoose.model('User', UserSchema);
