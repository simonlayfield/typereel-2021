const gulp = require('gulp');
const postcss = require('gulp-postcss');
const postcssNesting = require('postcss-nesting');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');

gulp.task('css', (done) => {
  gulp.src('src/css/**/*')
  .pipe(
    postcss([
      postcssNesting()
    ])
    )
    .pipe(concat('style.css'))
    .pipe(cleanCSS({compatibility: 'ie11'}))
    .pipe(gulp.dest('static/css'));
  done();
});

gulp.task('watch', (done) => {
  gulp.watch(['src/css/**/*'], gulp.series('css'));
  done();
});

gulp.task("default", gulp.series("watch"));